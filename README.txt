DESCRIPTION
===========

An Ubercart payment gateway module for the TransFirst payment processor.


CONFIGURATION
=============

Navigate to admin/store/settings/payment and configure to your gateway settings.


AUTHOR
============

Shane Graham "turbogeek" (http://drupal.org/user/266729)
http://www.deckfifty.com